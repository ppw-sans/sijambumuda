coverage==4.5.3
dj-database-url==0.5.0
Django==2.1.7
django-widget-tweaks==1.4.3
gunicorn==19.9.0
Pillow==5.4.1
psycopg2-binary==2.7.7
pytz==2018.9
whitenoise==4.1.2
social-auth-app-django