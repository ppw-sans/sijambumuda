from django.db import models
from django.contrib.auth.models import User
from jambu_muda.models import Book

# Create your models here.

class BookBorrowHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()
    return_date = models.DateField()