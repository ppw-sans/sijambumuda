from django.contrib.auth.models import User
from django.http import HttpRequest
from django.test import Client, TestCase, LiveServerTestCase
from django.urls import resolve
from .views import riwayat

# Create your tests here.

class RiwayatPageTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        self.user = User.objects.create_user(**self.credentials).save()
        self.client = Client()
        self.client.login(**self.credentials)

    def test_riwayat_url_is_exist(self):
        response = self.client.get('/riwayat/')
        self.assertEqual(response.status_code, 200)

    def test_nonexistent_url(self):
        response = self.client.get('/riwayat/404/')
        self.assertEqual(response.status_code, 404)

    def test_riwayat_page_using_riwayat_template(self):
        response = self.client.get('/riwayat/')
        self.assertTemplateUsed(response, 'book_count/riwayat.html')

    def test_riwayat_page_using_riwayat_view(self):
        found = resolve('/riwayat/')
        self.assertEqual(found.func, riwayat)
