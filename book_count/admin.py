from django.contrib import admin
from .models import BookBorrowHistory

# Register your models here.

class BookBorrowHistoryAdmin(admin.ModelAdmin):
    list_display = [field.name for field in BookBorrowHistory._meta.get_fields()]

admin.site.register(BookBorrowHistory, BookBorrowHistoryAdmin)