from django.urls import path
from .views import riwayat, riwayat_data

app_name = 'book_count'

urlpatterns = [
    path('', riwayat, name='riwayat'),
    path('data', riwayat_data, name='riwayat_data'),
]