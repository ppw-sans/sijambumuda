from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from .models import BookBorrowHistory
from datetime import timedelta
import json

# Create your views here.

@login_required
def riwayat(request):
    return render(request,'book_count/riwayat.html', {'user': request.user})

@login_required
def riwayat_data(request):
    if request.is_ajax():
        borrowed_books = request.user.bookborrowhistory_set.filter(user=request.user).order_by('-date')
        print(len(borrowed_books))
        data = dict()

        for idx, borrowed_book in enumerate(borrowed_books):
            data[idx] = {
                'id': borrowed_book.book.id_number,
                'cover': borrowed_book.book.cover_base64,
                'title': borrowed_book.book.title,
                'category': borrowed_book.book.get_category_display(),
                'author': borrowed_book.book.author,
                'publisher': borrowed_book.book.publisher,
                'date': str(borrowed_book.date),
                'return_date': str(borrowed_book.date + timedelta(weeks=2)),
            }

        data = json.dumps(data)
        return JsonResponse(data, safe=False)