from django.apps import AppConfig


class BookCountConfig(AppConfig):
    name = 'book_count'
