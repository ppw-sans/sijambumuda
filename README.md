# Tugas PPW Kelompok 5 🔥🔥

## Pilihan Tugas: JAMBU MUDA 🍐

Link: https://jambu-muda.herokuapp.com

### Stacks

Jambu Muda (_young guava_ in Indonesian) is web application that functions as an e-Library. Its aim is to be an easy-to-use, powerful, yet down to earth website that awakens literature awareness towards the young generation.

**Features**:
Book renting
Testimonials
Rent display
Adding-removing books **admin**
Modifying book quota **admin**

### Stacks

- Django
- Materialize
- Jquery

### Contributor

- Jonathan Filbert Lisyanto (1806191692)
- Faisal Hanif (1806133830)
- ~~Shahnaz Estee Laseidha (1606883146)~~
- Erania Siti Rajisa (1706039401)

### Features contributed

**Jonathan Filbert**

- Designed the whole User Interface and the userflow for all activities to ensure consistency and usability in accord of the 8 Golden Rules of UI Design.
- Prepared the front end templates to be manipulated by Django
- Implemented Google oAuth to login to the site
- Implemented various login cases to avoid duplicate account by traditional login and Google login
- Implemented book quota features and refined the borrowing logic.
- Refined the book borrowing logics to avoid user borrowing books with 0 quota
- Testing
- Readme

**Faisal Hanif**

- Implemented Borrowing features that let users borrow a book and be displayed on the site
- Implemented traditional sign up and log in features
- Implemented overall borrowing cards using AJAX
- Implemented book adding features by admin
- Implemented custom admin model to ensure maximum flexibility that suits the features
- Implemented custom form validation method
- Getting the distinct borrower data
- Testing

**Erania Siti Rajisa**

- Implemented Testimonial features
- Implemented Preloader on the site

**Status Coverage** 🛎
[![coverage report](https://gitlab.com/ppw-sans/sijambumuda/badges/master/coverage.svg)](https://gitlab.com/ppw-sans/sijambumuda/commits/master)

**Status Pipeline** 📍
[![pipeline status](https://gitlab.com/ppw-sans/sijambumuda/badges/master/pipeline.svg)](https://gitlab.com/ppw-sans/sijambumuda/commits/master)
