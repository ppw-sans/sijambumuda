from django.urls import path
from . import views
from django.conf.urls import include

app_name = 'jambu_muda'

urlpatterns = [
    path('', views.index, name='index'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('signup/', views.signup, name='signup'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('cari/', views.cari, name='cari'),
    path('check/', views.checker, name="check"),
    path('pinjam/', views.pinjam, name='pinjam'),
    path('peminjaman/', views.peminjaman, name='peminjaman'),
    path('complete/', views.complete, name="complete"),
    path('tambah_buku/', views.tambah_buku, name='tambah_buku'),
    path('testimoni/', views.testimoni, name='testimoni'),
    path('auth/', include('social_django.urls', namespace='social'))
]
