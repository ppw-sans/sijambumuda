from django.apps import AppConfig


class JambuMudaConfig(AppConfig):
    name = 'jambu_muda'
