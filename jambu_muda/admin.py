from django.contrib import admin
from .models import Book, BookBorrow, UserProfile, Testimony

# Register your models here.


class BookAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Book._meta.get_fields() if field.name != 'borrow' and field.name != 'bookborrow' and field.name != 'bookborrowhistory']

class BookBorrowAdmin(admin.ModelAdmin):
    list_display = [field.name for field in BookBorrow._meta.get_fields()]


class UserProfileAdmin(admin.ModelAdmin):
    list_display = [field.name for field in UserProfile._meta.get_fields(
    ) if field.name != 'bookborrow']


admin.site.register(Book, BookAdmin)
admin.site.register(BookBorrow, BookBorrowAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Testimony)
