from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client
from . import views
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase
from . import models
from .forms import *

# Create your tests here.

class SampleTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret',
            'email': 'a@b.com'}
        self.user = User.objects.create_superuser(**self.credentials).save()
        self.client = Client()
        self.client.login(**self.credentials)

    def test_homepage_views(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_homepage_response(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_404(self):
        response = Client().get('/404')
        self.assertEqual(response.status_code,404)

    def test_cari(self):
        response = self.client.get('/cari/')
        self.assertEqual(response.status_code, 200)

    def test_get_signup(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_post_signup(self):
        response = Client().post('/signup/',{'username':'JonathanFilbertKeren','first_name':'Jonathan','lat_name':'Filbert','password1':'admin123456','password2':'admin123456','email':'jonathanzero@gmail.com'},)
        self.assertIn('JonathanFilbertKeren',response.content.decode())

    def test_get_login(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_post_login(self):
        response = Client().post('/login/',{
            'username':'jofil',
            'password':'admin123456',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

        response = Client().post('/login/',{
            'username':'testuser',
            'password':'secret',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        response = self.client.get('/logout/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_get_peminjaman(self):
        response = self.client.get('/peminjaman/')
        self.assertEqual(response.status_code, 200)

    def test_get_pinjam(self):
        response = self.client.get('/pinjam/')
        self.assertEqual(response.status_code, 200)

    def test_post_pinjam(self):
        import datetime
        book = Book.objects.create(
            id_number = 'jofil',
            category = 'jofil',
            title = 'jofil',
            author = 'jofil',
            publisher = 'jofil',
            synopsis = 'jofil',
            cover_base64 = 'jofil',
            quota = 1,
        )
        self.assertEqual(str(book), '[jofil] jofil')

        response = self.client.post('/pinjam/',{
            'book':book.pk,
            'date':'2019-01-01',
        }, follow=True)
        borrower = book.get_borrow
        self.assertEqual(response.status_code, 200)

    def test_get_tambah_buku(self):
        response = self.client.get('/tambah_buku/')
        self.assertEqual(response.status_code, 200)

    def test_post_tambah_buku(self):
        from django.core.files.uploadedfile import SimpleUploadedFile
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        image = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        self.client.post('/tambah_buku/', {'id_number':'1', 'category':'science', 'title':'1', 'author':'1', 'publisher':'1', 'synopsis':'1', 'quota':'1', 'cover':image})

    def test_get_testimoni(self):
        response = self.client.get('/testimoni/')
        self.assertEqual(response.status_code, 200)

    def test_post_testimoni(self):
        response = self.client.post('/testimoni/',{
            'comment': 'test',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_user_form(self):
        c = Client()
        name = 'user'
        password = 'coba12345'
        email = 'user@user.com'
        c.post('/signup/', {'username': name, 'first_name': name, 'last_name': name,
                            'password1': password, 'password2': password, 'email': email,
                            'id_number': name, 'address': name})

    # def test_login(self):
    #     response = Client().get('/testimoni/')
    #     self.assertEqual(response.status_code,302)

    # def test_pinjam(self):
    #     response = Client().get('/pinjam/')
    #     self.assertEqual(response.status_code,302)

