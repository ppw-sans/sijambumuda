from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Book, BookBorrow, UserProfile, Testimony



class UserForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=150, required=True)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'password1', 'password2', 'email')

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Nama depan'
        self.fields['last_name'].label = 'Nama belakang'

        for fieldname in UserForm.Meta.fields:
            self.fields[fieldname].help_text = None


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('id_number', 'address')
        labels = {'id_number': 'Nomor identitas', 'address': 'Alamat'}

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['address'].widget.attrs['class'] = 'materialize-textarea'


class LoginForm(forms.Form):
    username = forms.CharField(label='Username')
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'autofocus': True})

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')

        if username and password:
            self.user = authenticate(username=username, password=password)
            if self.user:
                self.login_allowed = True
            else:
                self.add_error('username', 'Username atau password salah.')
                self.add_error('password', 'Username atau password salah.')


class BookForm(forms.ModelForm):
    cover = forms.ImageField()

    class Meta:
        model = Book
        fields = ('id_number', 'category', 'title',
                  'author', 'publisher', 'synopsis','quota', 'cover')

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        self.fields['synopsis'].widget.attrs['class'] = 'materialize-textarea'
        self.fields['id_number'].widget.attrs['autofocus'] = True
        self.fields['cover'].widget.attrs['accept'] = '.jpg,.jpeg'

class BorrowBookForm(forms.Form):
    book = forms.ModelChoiceField(
        label="Nomor Buku", queryset=Book.objects.all().exclude(quota=0))
    date = forms.DateField(
        label="Tanggal Pinjam (yyyy-mm-dd)",
        input_formats=['%Y-%m-%d'],
        widget=forms.DateTimeInput(attrs={'class': 'datepicker', }),
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(BorrowBookForm, self).__init__(*args, **kwargs)

class TestiForm(forms.ModelForm):
    class Meta:
        model = Testimony
        fields = ('comment',)
        labels = {'comment': 'Masukkan Testimoni anda'}

