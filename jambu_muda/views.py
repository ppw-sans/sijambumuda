from datetime import datetime, timedelta
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from .forms import BookForm, BorrowBookForm, LoginForm, UserForm, UserProfileForm, TestiForm
from .models import Book, BookBorrow, UserProfile, Testimony
from book_count.models import BookBorrowHistory
from django.db.models import Q


# Create your views here.

def anonymous_required(view, redirect_to='/dashboard'):
    wrapper = user_passes_test(
        lambda u: u.is_anonymous,
        login_url=redirect_to,
    )
    return wrapper(view)


def superuser_required(view, redirect_to='/'):
    wrapper = user_passes_test(
        lambda u: u.is_superuser,
        login_url=redirect_to,
    )
    return wrapper(view)


@anonymous_required
def index(request):
    testimoni = Testimony.objects.all()
    return render(request, 'jambu_muda/index.html', {'user': request.user,
                                                     'user_count': User.objects.all().count(),
                                                     'book_count': Book.objects.all().count(),
                                                     'testimoni': testimoni,
                                                     })


@anonymous_required
def signup(request):
    if request.method == 'POST':
        uform = UserForm(request.POST)
        pform = UserProfileForm(request.POST)
        if uform.is_valid() and pform.is_valid():
            user = uform.save()
            profile = pform.save(commit=False)
            profile.user = user
            profile.save()
            return redirect('jambu_muda:login')
    else:
        uform = UserForm()
        pform = UserProfileForm()
    return render(request, 'jambu_muda/signup.html', {'uform': uform, 'pform': pform})


@anonymous_required
def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            if form.login_allowed:
                auth_login(request, form.user)
                return redirect('jambu_muda:dashboard')
    else:
        form = LoginForm()

    return render(request, 'jambu_muda/login.html', {'form': form})


@login_required
def logout(request):
    auth_logout(request)
    return redirect('jambu_muda:index')


@login_required
def cari(request):
    return render(request, 'jambu_muda/cari.html', {'user': request.user, 'books': Book.objects.all()})


@login_required
def pinjam(request):
    if request.method == 'POST':
        form = BorrowBookForm(request.POST, request=request)
        if form.is_valid():
            data = {'user':request.user, 'book':form.cleaned_data['book'], 'date':form.cleaned_data['date']}
            return_date = data['date'] + timedelta(weeks=2)
            BookBorrow(**data).save()
            BookBorrowHistory(return_date=return_date, **data).save()
            bukuDiPinjam = Book.objects.get(id_number=data['book'].id_number)
            if bukuDiPinjam.quota > 0:
                bukuDiPinjam.quota -= 1
            bukuDiPinjam.save()
            return redirect('jambu_muda:dashboard')
    else:
        form = BorrowBookForm(request=request)
    return render(request, 'jambu_muda/pinjam.html', {'user': request.user, 'form': form})


@login_required
def peminjaman(request):
    BookBorrow.objects.filter(
        date__lt=datetime.today() - timedelta(weeks=2)).delete()
    return render(request, 'jambu_muda/peminjaman.html', {'user': request.user,
                                                          'books': Book.objects.filter(bookborrow__date__gte=datetime.today() - timedelta(weeks=2)).distinct()})


@login_required
def dashboard(request):
    BookBorrow.objects.filter(
        date__lt=datetime.today() - timedelta(weeks=2)).delete()
    return render(request, 'jambu_muda/dashboard.html', {'user': request.user,
                                                         'borrowed_books': request.user.bookborrow_set.filter(user=request.user, date__gte=datetime.today() - timedelta(weeks=2))})


@superuser_required
def tambah_buku(request):
    import base64

    if request.method == 'POST':
        form = BookForm(request.POST, request.FILES)
        if form.is_valid():
            with form.cleaned_data['cover'].open("rb") as cover:
                cover_base64 = 'data:image/jpg;base64,{}'.format(
                    base64.b64encode(cover.read()).decode())

                book = form.save()
                book.cover_base64 = cover_base64
                book.save()
                return redirect('jambu_muda:tambah_buku')
    else:
        form = BookForm()
    return render(request, 'jambu_muda/tambah_buku.html', {'user': request.user, 'form': form})


@login_required
def checker(request):
    print("masuk checker")
    try:
        print("masuk try")
        user = UserProfile.objects.all().filter(user=request.user)
        return redirect('jambu_muda:pinjam')
    except:
        print("masuk except")
        return redirect('jambu_muda:complete')


@login_required
def complete(request):
    if request.method == 'POST':
        pform = UserProfileForm(request.POST)
        if pform.is_valid():
            profile = pform.save(commit=False)
            profile.user = request.user
            profile.save()
            return redirect('jambu_muda:dashboard')
    else:
        pform = UserProfileForm()
    return render(request, 'jambu_muda/complete.html', {'pform': pform})

@login_required
def testimoni(request):
    if request.method == 'POST':
        form = TestiForm(request.POST)
        testimony = form.save(commit=False)
        testimony.user = request.user
        testimony.save()
        return redirect('jambu_muda:dashboard')
    else:
        form = TestiForm()
    return render(request, 'jambu_muda/testimoni.html', {'testimony_list': Testimony.objects.all(),'form':form})
