from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Book(models.Model):
    CATEGORY_CHOICES = (
        ('science', 'Science'),
        ('scifi', 'Science Fiction'),
        ('hobby', 'Hobby'),
        ('other', 'Other'),
    )
    id_number = models.CharField(max_length=20, unique=True)
    quota = models.IntegerField()
    category = models.CharField(max_length=20, choices=CATEGORY_CHOICES)
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    publisher = models.CharField(max_length=200)
    synopsis = models.TextField()
    borrow = models.ManyToManyField(User, blank=True, through='BookBorrow')
    cover_base64 = models.TextField(blank=True)

    @property
    def get_borrow(self):
        return self.borrow.distinct()

    def __str__(self):
        return '[{}] {}'.format(self.id_number, self.title)


class BookBorrow(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()


class Testimony(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField()


class UserProfile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile')
    id_number = models.CharField(max_length=20, unique=True)
    address = models.TextField()


User._meta.get_field('email')._unique = True
